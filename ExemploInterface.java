import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ExemploInterface {

	private JFrame mainFrame;
	private JLabel headerLabel;
	private JPanel controlPanel;
	private JLabel statusLabel;
	
	public ExemploInterface() {
		preparaGUI();
		criaPanel();
	}
	
	public void preparaGUI() {
		mainFrame = new JFrame("Exemplo de Interface em Java");
		mainFrame.setSize(400, 500);
		mainFrame.setLocation(2000, 200);
		mainFrame.setLayout(new GridLayout(3,1));
		
		mainFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowEvent) {
				System.exit(0);
			}
		});
		
		headerLabel = new JLabel("Header Label", JLabel.CENTER);
		statusLabel = new JLabel("Status Label", JLabel.CENTER);
		statusLabel.setSize(350,100);
		
		controlPanel = new JPanel();
		controlPanel.setLayout(new FlowLayout());
		
		mainFrame.add(headerLabel);
		mainFrame.add(controlPanel);
		mainFrame.add(statusLabel);
		
		mainFrame.setVisible(true);
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void criaPanel() {
		JButton okButton = new JButton("Ok");
		JButton submeterButton = new JButton("Submeter");
		JButton cancelarButton = new JButton("Cancelar");
		
		okButton.setActionCommand("Ok");
		submeterButton.setActionCommand("Submeter");
		cancelarButton.setActionCommand("Cancelar");
		
		okButton.addActionListener(new TratadorDeEventoDoBotao());
		submeterButton.addActionListener(new TratadorDeEventoDoBotao());
		cancelarButton.addActionListener(new TratadorDeEventoDoBotao());
		
		controlPanel.add(okButton);
		controlPanel.add(submeterButton);
		controlPanel.add(cancelarButton);
		
		headerLabel.setText("Clique em um dos botões!");
		
	}
	
	private class TratadorDeEventoDoBotao implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			String comando = e.getActionCommand();
			if(comando.equals("Ok")) {
				statusLabel.setText("Botão Ok pressionado!");
			}
			else if(comando.equals("Submeter")) {
				statusLabel.setText("Botão Submeter pressionado!");
			}
			else {
				statusLabel.setText("Botão Cancelar pressionado!");
			}
		}
	}
	
	public static void main(String[] args) {
		ExemploInterface exemplo = new ExemploInterface();
	}
	
}
